package com.app.sigam;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class DBController extends SQLiteOpenHelper {


    private static String TAG = "DataBaseHelper"; // Tag just for the LogCat window
    //destination path (location) of our database on device
    private static String DB_PATH = "";
    private static String DB_NAME = "sigam.db";// Database name with extenssion
    private SQLiteDatabase mDataBase;
    private final Context mContext;

    public DBController(Context context) {
        super(context, DB_NAME, null, 1);// 1? Its database Version
        if (android.os.Build.VERSION.SDK_INT >= 17) {
            DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
        } else {
            DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
        }
        this.mContext = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        //If the database does not exist, copy it from the assets.

    }

    public void createDataBase() {
        boolean mDataBaseExist = checkDataBase();
        if (!mDataBaseExist) {
            this.getReadableDatabase();
            this.close();
            try {
                //Copy the database from assests
                copyDataBase();
                Log.e(TAG, "createDatabase database created");
            } catch (IOException mIOException) {
                SQLiteDatabase sqlSuport = SQLiteDatabase.openDatabase(DB_PATH + DB_NAME, null, SQLiteDatabase.CREATE_IF_NECESSARY);


                String SQL = "CREATE TABLE 'veiculos' (" +
                        "'_ID' INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT," +
                        "'PLACA' TEXT NOT NULL UNIQUE," +
                        "'VEICULO' TEXT," +
                        "'STATUS' TEXT );";


                sqlSuport.execSQL(SQL);

                SQL =
                        "INSERT INTO 'main'.'veiculos' ('_ID', 'PLACA', 'VEICULO', 'STATUS') VALUES ('1', 'OYS3451', 'RENAULT/SANDERO EXPR 1O 2014/2015 - PRETA', 'COM DEBITOS')," +
                                " ('2', 'PCE2864', 'YAMAHA/XTZ250 LANDER 2017/2017 - VERMELHA', 'COM DEBITOS')" +
                                ",('3', 'PEN4003', 'FIAT/DOBLO ADV. 1.8 FLEX 2011/2011 - VERDE', 'SEM DEBITOS')" +
                                ",('4', 'MYY4044', 'I/TOYOTA HILUXSW4 SRV4X4 2006/2006 - PRETA', 'SEM DEBITOS')" +
                                ",('5', 'BEE4R22', 'GM/CRUZE LTZ 1.4T 2019/2019 - BRANCO', 'AVISO DE ROUBO')" +
                                ", ('6', 'BRA20E4', 'HONDA/CRF 150 2018/2019 - AZUL', 'SEM DEBITOS')" +
                                ",('7', 'BE028AB', 'Mercedes Benz/C180 2018/2019 - Preto', 'COM DEBITOS');";

                sqlSuport.execSQL(SQL);


                SQL = "CREATE TABLE 'ipva' ('fk_id'	INTEGER," +
                        "'ANO'	INTEGER," +
                        "'DEBITO'	INTEGER );";

                sqlSuport.execSQL(SQL);



                SQL =
                        "INSERT INTO 'main'.'ipva' ('fk_id', 'ANO', 'DEBITO') VALUES ('1', '2018', '1350'),"+
                        "('2', '2016', '2250'),('2', '2017', '2340')," +
                                "('2', '2018', '2120'),('7', '2018', '7350');";

                sqlSuport.execSQL(SQL);

                sqlSuport.close();

                Log.i(TAG, "CRIADA DA EXCEÇÃO");
//                throw new Error("ErrorCopyingDataBase");

            }
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }

    //Check that the database exists here: /data/data/your package/databases/Da Name
    private boolean checkDataBase() {
        File dbFile = new File(DB_PATH + DB_NAME);
        //Log.v("dbFile", dbFile + "   "+ dbFile.exists());
        return dbFile.exists();
    }

    //Copy the database from assets
    private void copyDataBase() throws IOException {
        InputStream mInput = mContext.getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream mOutput = new FileOutputStream(outFileName);

        byte[] mBuffer = new byte[1024];
        int mLength;

        while ((mLength = mInput.read(mBuffer)) > 0) {
            mOutput.write(mBuffer, 0, mLength);
        }
        mOutput.flush();
        mOutput.close();
        mInput.close();
    }

    //Open the database, so we can query it
    public boolean openDataBase() throws SQLException {
        String mPath = DB_PATH + DB_NAME;
        //Log.v("mPath", mPath);
        mDataBase = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.CREATE_IF_NECESSARY);
        //mDataBase = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
        return mDataBase != null;
    }


    @Override
    public synchronized void close() {
        if (mDataBase != null)
            mDataBase.close();
        super.close();
    }

}
