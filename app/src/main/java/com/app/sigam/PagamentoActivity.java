package com.app.sigam;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.github.rtoshiro.util.format.SimpleMaskFormatter;
import com.github.rtoshiro.util.format.text.MaskTextWatcher;

import java.util.ArrayList;
import java.util.List;

public class PagamentoActivity extends AppCompatActivity {
    ListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pagamento);

        EditText numeroCartao, nomeCompleto, data, cod;
        numeroCartao = findViewById(R.id.tv_cardnumber);
        nomeCompleto = findViewById(R.id.tv_nome_c);

//        DBAdapter adapter = new DBAdapter(getApplicationContext());
//        adapter.open();

//        int debitosTotais = adapter.getDebitosTotal(veiculoId);
        int debitosTotais = getIntent().getIntExtra("total", 1000);
        data = findViewById(R.id.tv_validade);
        cod = findViewById(R.id.tv_cod_seg);
        data.setImeOptions(EditorInfo.IME_ACTION_NEXT);
        cod.setImeOptions(EditorInfo.IME_ACTION_DONE);

        if(cod.getTextSize() == 3){
            cod.clearFocus();
        }


        Button pagarCartao = findViewById(R.id.button_pagar_cartao);
        pagarCartao.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getApplicationContext(), DialogPagamento.class)
                .addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY));
            }
        });

        SimpleMaskFormatter smf = new SimpleMaskFormatter("NNNN.NNNN.NNNN.NNNN");
        MaskTextWatcher mtw = new MaskTextWatcher(numeroCartao, smf);
        numeroCartao.addTextChangedListener(mtw);

        SimpleMaskFormatter smf2 = new SimpleMaskFormatter("NN/NNNN");
        MaskTextWatcher mtw2 = new MaskTextWatcher(data, smf2);
        data.addTextChangedListener(mtw2);

        listView = findViewById(R.id.pagamento_cartao);

        List<String>  parcelas = new ArrayList<>();

        int parcela2 = debitosTotais / 2;
        int parcela3 = debitosTotais / 3;
        int parcela4 = debitosTotais / 4;
        int parcela5 = debitosTotais / 5;

        parcelas.add("1 Parcela de " + debitosTotais);
        parcelas.add("2 Parcela de " + parcela2);
        parcelas.add("3 Parcela de " + parcela3);
        parcelas.add("4 Parcela de " + parcela4);
        parcelas.add("5 Parcela de " + parcela5);


        listView.setAdapter(new ParcelasCartaoListAdapter(this, parcelas));

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                view.setSelected(true);
                InputMethodManager imm = (InputMethodManager) getApplication().getSystemService(Activity.INPUT_METHOD_SERVICE);
                //Find the currently focused view, so we can grab the correct window token from it.

                assert imm != null;
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        });
    }

}
