package com.app.sigam;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class ParcelasListAdapter extends BaseAdapter {

    Activity activity;
    List<String> stringList;

    public ParcelasListAdapter(Activity activity, List<String> list){
        this.activity = activity;
        this.stringList = list;
    }

    @Override
    public int getCount() {
        return stringList.size();
    }

    @Override
    public Object getItem(int position) {
        return stringList.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        @SuppressLint("ViewHolder") View v = activity.getLayoutInflater()
                .inflate(R.layout.list_item_parcelas, viewGroup, false);

        TextView textoParcelas, valorParcerlas;

        textoParcelas = v.findViewById(R.id.txt_item_parcela);
        valorParcerlas = v.findViewById(R.id.txt_item_valor_parcela);

        String debitoEAno = stringList.get(i);

        Log.i("Details-->", debitoEAno);
        String ano = debitoEAno.substring(0,4);
        String debito = debitoEAno.substring(4);

        textoParcelas.setText("IPVA " + ano);
        valorParcerlas.setText("R$ " + debito + ",00");

        return v;
    }
}
