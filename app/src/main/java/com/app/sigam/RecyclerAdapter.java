package com.app.sigam;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class RecyclerAdapter extends RecyclerView.Adapter {

    private List<Carro> listaCarro;
    private Context context;

    RecyclerAdapter(List<Carro> list, Context context) {
        this.listaCarro = list;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_item_image, viewGroup, false);
        return new ViewHolder(itemView);

    }
    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is =context.getAssets().open("csvjson.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, final int position) {
        final ViewHolder v = (ViewHolder) viewHolder;
        List<Carro> formList = new ArrayList<>();

        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("CARROS");


            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                Carro c = new Carro();
//                Log.d("Details-->", jo_inside.getString("PLACA"));

                c.setId(jo_inside.getInt("ID"));
                c.setResult(jo_inside.getString("PLACA"));
                c.setMarcaModelo(jo_inside.getString("MARCA"));
                c.setSituacao(jo_inside.getString("STATUS"));
                c.setTotal(jo_inside.getInt("TOTAL"));

                //Add your values in your `ArrayList` as below:
                formList.add(c);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        List<Carro> json = new ArrayList<>(formList);

        Carro c = listaCarro.get(position);

        for (Carro carroAux: json) {

            Log.i("result", carroAux.getResult() + " " + carroAux.getSituacao()
                    + " "+ carroAux.getMarcaModelo() + " Comparação: " + c.getResult());

            if(c.getResult().equals(carroAux.getResult())
                    && carroAux.getSituacao().equals("COM")){

                v.condition.setImageResource(R.drawable.rectangle_image_red);
                MainActivity.listaLimpa.get(position).setSituacao("COM DEBITOS");
                MainActivity.listaLimpa.get(position).setMarcaModelo(carroAux.getMarcaModelo());
                MainActivity.listaLimpa.get(position).setId(carroAux.getId());

            }

            if(c.getResult().equals(carroAux.getResult())
                    && carroAux.getSituacao().equals("AVISO")){

                v.condition.setImageResource(R.drawable.rectangle_image_red);
                MainActivity.listaLimpa.get(position).setSituacao("AVISO DE ROUBO");
                MainActivity.listaLimpa.get(position).setMarcaModelo(carroAux.getMarcaModelo());
                MainActivity.listaLimpa.get(position).setId(carroAux.getId());

            }

            if(c.getResult().equals(carroAux.getResult())
                    && carroAux.getSituacao().equals("SEM")){

                v.condition.setImageResource(R.drawable.rectangle_image_green);
                MainActivity.listaLimpa.get(position).setSituacao("SEM DEBITOS");
                MainActivity.listaLimpa.get(position).setMarcaModelo(carroAux.getMarcaModelo());
                MainActivity.listaLimpa.get(position).setId(carroAux.getId());

            }

        }

        v.imagem.setImageBitmap(c.getCutoutImage().getBitmap());

        v.imagem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MainActivity.updateView(position);
            }
        });

//        dbAdapter.close();

    }

    @Override
    public int getItemCount() {
        return listaCarro.size();
    }


    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private ImageView imagem;
        private ImageView condition;

        private OnImageItemClickListener itemClickListener;

        ViewHolder(@NonNull View itemView) {
            super(itemView);

            imagem = itemView.findViewById(R.id.result_image);
            condition = itemView.findViewById(R.id.indicador_condicao);
        }

        @Override
        public void onClick(View v) {
            itemClickListener.onClick(v, getAdapterPosition(), false);
        }

        void setItemClickListener(OnImageItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }
    }


}
