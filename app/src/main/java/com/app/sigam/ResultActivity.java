package com.app.sigam;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class ResultActivity extends AppCompatActivity{

    TabLayout tabLayout;
    ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        Bundle extras = getIntent().getExtras();

        String placa = extras.getString("placa");
        String marcaModeloStr = extras.getString("marcaModelo");
        int position = extras.getInt("listIndex");
        final int veiculoId = extras.getInt("id");

        tabLayout = findViewById(R.id.tabLayout);
        viewPager = findViewById(R.id.result_view_pager);
        Bitmap imagem = MainActivity.listaLimpa.get(position).getCutoutImage().getBitmap();

        TextView cidadeEstado = findViewById(R.id.result_cidade_estado);
        TextView marcaModelo = findViewById(R.id.result_carro_modelo);

        marcaModelo.setText(marcaModeloStr);
        cidadeEstado.setText("Cidade - Estado -> "+ placa);

        ImageView i = findViewById(R.id.result_image_result);

        if (imagem != null) {
            i.setImageBitmap(imagem);
        }

        Fragment debitosFragment = new DebitosFragment();
        ((DebitosFragment) debitosFragment).setVeiculoId(veiculoId);

        AbasAdapter adapter = new AbasAdapter(getSupportFragmentManager());
        adapter.adicionar(debitosFragment, "Situação");
        adapter.adicionar(new DebitosFragment(), "Veiculo");
        adapter.adicionar(new CondutorFragment(), "Condutor");

        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);

    }

    public class AbasAdapter extends FragmentPagerAdapter {

        private List<Fragment> fragments = new ArrayList<>();
        private List<String> titulos = new ArrayList<>();

        public AbasAdapter(FragmentManager fm) {
            super(fm);
        }

        public void adicionar(Fragment fragment, String tituloAba){
            this.fragments.add(fragment);
            this.titulos.add(tituloAba);
        }

        @Override
        public Fragment getItem(int position) {
            return this.fragments.get(position);
        }

        @Override
        public int getCount() {
            return this.fragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position){
            return this.titulos.get(position);
        }
    }
}
