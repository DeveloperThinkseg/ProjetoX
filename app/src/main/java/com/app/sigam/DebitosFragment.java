package com.app.sigam;


import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class DebitosFragment extends Fragment {


    private List<Debitos> debitosList;

    public DebitosFragment() {
        // Required empty public constructor
    }

    Carro c;
    Debitos d;
    private int veiculoId;
    private int valorTotal;

    public void setVeiculoId(int id) {
        this.veiculoId = id;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View view = inflater.inflate(R.layout.viewpager_debitos, container, false);
        TextView t = view.findViewById(R.id.txt_item_valor_parcela);

        List<Carro> formList = new ArrayList<>();

        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray m_jArry = obj.getJSONArray("CARROS");

            for (int i = 0; i < m_jArry.length(); i++) {
                JSONObject jo_inside = m_jArry.getJSONObject(i);
                c = new Carro();
//                Log.d("Details-->", jo_inside.getString("PLACA"));
                JSONArray ja;

                c.setId(jo_inside.getInt("ID"));
                if (veiculoId == c.getId()) {
                    debitosList = new ArrayList<>();

                    c.setResult(jo_inside.getString("PLACA"));
                    c.setTotal(jo_inside.getInt("TOTAL"));
                    c.setList(debitosList);

                    Log.d("Details--> ID", String.valueOf(veiculoId));

                    ja = jo_inside.getJSONArray("DEBITOS");
                    valorTotal = c.getTotal();

                    for (int j = 0; j < ja.length(); j++) {
                        d = new Debitos();

                        d.setAno(ja.getJSONObject(j).getInt("ANO"));
                        d.setValor(ja.getJSONObject(j).getInt("VALOR"));

                        c.getList().add(d);

                        Log.d("Details--> ANO", d.getAno() + "");
                        Log.d("Details--> DEBITO", d.getValor() + "");
                    }
//                    //Add your values in your `ArrayList` as below:
                    formList.add(c);
                }

                //Add your values in your `ArrayList` as below:
//                formList.add(c);
            }
        } catch (JSONException e) {
            Toast.makeText(getContext(), e.getMessage(), Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        final List<String> l = new ArrayList<>();

        for (int i = 0; i < formList.size(); i++) {
            for (int j = 0; j < debitosList.size(); j++) {
                l.add(debitosList.get(j).getAno() + "" + debitosList.get(j).getValor());
            }
//            Log.d("Details", String.valueOf(debitosList.get(i).getValor()));
        }

        ImageView imgPagar = view.findViewById(R.id.img_pagar);
        ImageView imgAutuar = view.findViewById(R.id.img_autuar);
        ListView listView = view.findViewById(R.id.result_list_parcelas);

        listView.setAdapter(new ParcelasListAdapter(getActivity(), l));

        t.setText("R$ " + valorTotal + ",00");


        AlertDialog.Builder alertB = new AlertDialog.Builder(getContext());
        alertB.setTitle("Autuação de veículo");
        alertB.setMessage("Veículo Autuado");
        alertB.setCancelable(false);

        alertB.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                getActivity().finish();
            }
        });

        final AlertDialog alert = alertB.create();

        imgAutuar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                alert.show();
            }
        });

        imgPagar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getContext(), PagamentoActivity.class)
                        .putExtra("total", valorTotal)
                        .setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY));

            }
        });

        return view;
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getAssets().open("csvjson.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }
}
