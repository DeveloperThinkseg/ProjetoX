package com.app.sigam;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

public class ParcelasCartaoListAdapter extends BaseAdapter {

    Activity activity;
    List<String> stringList;

    public ParcelasCartaoListAdapter(Activity activity, List<String> list){
        this.activity = activity;
        this.stringList = list;
    }

    @Override
    public int getCount() {
        return stringList.size();
    }

    @Override
    public Object getItem(int position) {
        return stringList.get(position);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

        View v = activity.getLayoutInflater()
                .inflate(R.layout.list_item_parcelas_cartao, viewGroup, false);

        TextView textoParcelas, valorParcerlas;
        textoParcelas = v.findViewById(R.id.txt_item_parcela_cartao);
        valorParcerlas = v.findViewById(R.id.txt_item_valor_parcela_cartao);

        String debitoEAno = stringList.get(i);

        String ano = debitoEAno.substring(0,13);
        String debito = debitoEAno.substring(13);

        textoParcelas.setText(ano);
        valorParcerlas.setText("R$ "+debito+",00");

        return v;
    }
}
