package com.app.sigam;

import java.util.List;

import at.nineyards.anyline.models.AnylineImage;

public class Carro {

    private String result, marcaModelo;
    private AnylineImage cutoutImage;
    private AnylineImage fullImage;
    String situacao;
    private int total;
    List<Debitos> list;

    public List<Debitos> getList() {
        return list;
    }

    public void setList(List<Debitos> list) {
        this.list = list;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    int id;

    public String getSituacao() {
        return situacao;
    }

    public void setSituacao(String situacao) {
        this.situacao = situacao;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public AnylineImage getCutoutImage() {
        return cutoutImage;
    }

    public void setCutoutImage(AnylineImage cutoutImage) {
        this.cutoutImage = cutoutImage;
    }

    public AnylineImage getFullImage() {
        return fullImage;
    }

    public void setFullImage(AnylineImage fullImage) {
        this.fullImage = fullImage;
    }

    @Override
    public boolean equals(Object obj) {

            if(obj instanceof Carro)
            {
                Carro temp = (Carro) obj;
                if(this.result.equals(temp.result))
                    return true;
            }
            return false;
    }

    @Override
    public int hashCode() {

        return (this.result.hashCode());
    }

    public String getMarcaModelo() {
        return marcaModelo;
    }

    public void setMarcaModelo(String marcaModelo) {
        this.marcaModelo = marcaModelo;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    int getTotal(){
       return  this.total;
    }

}
