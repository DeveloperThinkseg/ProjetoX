package com.app.sigam;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DBAdapter {

    protected static final String TAG = "DataAdapter";

    private final Context mContext;
    private SQLiteDatabase mDb;
    private DBController mDbHelper;

    public DBAdapter(Context context)
    {
        this.mContext = context;
        mDbHelper = new DBController(mContext);
    }

    public DBAdapter createDatabase() throws SQLException
    {
        mDbHelper.createDataBase();
        return this;
    }

    public DBAdapter open() throws SQLException{
        try
        {
            mDbHelper.openDataBase();
            mDbHelper.close();
            mDb = mDbHelper.getReadableDatabase();
        }
        catch (SQLException mSQLException)
        {
            Log.e(TAG, "open >>"+ mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    public void close()
    {
        mDbHelper.close();
    }

    public Cursor getTestData(){
        try
        {
            String sql ="SELECT * FROM veiuculos";

            Cursor mCur = mDb.rawQuery(sql, null);
            if (mCur!=null)
            {
                mCur.moveToNext();
            }
            return mCur;
        }
        catch (SQLException mSQLException)
        {
            Log.e(TAG, "getTestData >>"+ mSQLException.toString());
            throw mSQLException;
        }
    }

    public List<Carro> getPlaca() {

        List<Carro> recordsList = new ArrayList<>();

        // select query
        String sql = "SELECT _ID, PLACA, VEICULO, STATUS FROM veiculos";

        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Carro carro = new Carro();
                carro.setId(cursor.getInt(cursor.getColumnIndex("_ID")));
                carro.setResult(cursor.getString(cursor.getColumnIndex("PLACA")));
                carro.setSituacao(cursor.getString(cursor.getColumnIndex("STATUS")));
                carro.setMarcaModelo(cursor.getString(cursor.getColumnIndex("VEICULO")));
                // add to list
                recordsList.add(carro);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        // return the list of records
        return recordsList;
    }

    public List<Carro> getPlaca(String placa) {

        List<Carro> recordsList = new ArrayList<>();

        // select query
        String sql = "SELECT _ID, PLACA, VEICULO, STATUS FROM veiculos WHERE 'PLACA' = " + placa  +"";

        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Carro carro = new Carro();
                carro.setId(cursor.getInt(cursor.getColumnIndex("_ID")));
                carro.setResult(cursor.getString(cursor.getColumnIndex("PLACA")));
                carro.setSituacao(cursor.getString(cursor.getColumnIndex("STATUS")));
                carro.setMarcaModelo(cursor.getString(cursor.getColumnIndex("VEICULO")));
                // add to list
                recordsList.add(carro);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        // return the list of records
        return recordsList;
    }

    public List<String> getDebitos(int attr){

        List<String> recordsList = new ArrayList<>();

        // select query
        String sql = "SELECT ANO, DEBITO FROM ipva " +
                "INNER JOIN veiculos WHERE veiculos._ID = ipva.fk_id " +
                "AND veiculos._ID = '"+attr+"'";

        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                // int productId = Integer.parseInt(cursor.getString(cursor.getColumnIndex(fieldProductId)));
                String debitos = "";
                debitos += (cursor.getString(cursor.getColumnIndex("ANO")));
                debitos += (cursor.getString(cursor.getColumnIndex("DEBITO")));

                // add to list
                recordsList.add(debitos);

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        // return the list of records
        return recordsList;
    }

    public int getDebitosTotal(int attr){

         int total = 0;
        // select query
        String sql = "SELECT SUM(DEBITO) AS DEBITOS FROM ipva " +
                "INNER JOIN veiculos WHERE veiculos._ID = ipva.fk_id " +
                "AND veiculos._ID = '"+attr+"'";

        SQLiteDatabase db = mDbHelper.getReadableDatabase();

        // execute the query
        Cursor cursor = db.rawQuery(sql, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            total = (cursor.getInt(cursor.getColumnIndex("DEBITOS")));
        }

        cursor.close();
        db.close();

        // return the list of records
        return total;
    }

}